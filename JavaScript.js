// Fungsi validasi
function Validasi(event) {
    // Ambil nilai dari input dengan id "nama", "email", dan "pass"
    var nama = document.getElementById('nama').value;
    var email = document.getElementById('email').value;
    var pass = document.getElementById('pass').value;

    // Pengecekan untuk memastikan semua kolom diisi
    if (nama === '' || email === '' || pass === '') {
        alert("Harap isi semua kolom");
        event.preventDefault(); // Mencegah form submission
        return false;
    }

    // Validasi username
    if (nama !== 'Muhammad Rafi Hibatullah') {
        alert("Username yang anda masukkan salah");
        event.preventDefault(); // Mencegah form submission
        return false;
    }

    // Validasi email
    if (email !== 'rafihiba777@gmail.com') {
        alert("Email yang anda masukkan salah");
        event.preventDefault(); // Mencegah form submission
        return false;
    }

    // Validasi password
    if (pass !== 'rafi123') {
        alert("Password yang anda masukkan salah");
        event.preventDefault(); // Mencegah form submission
        return false;
    }

    // Jika semua validasi berhasil, tampilkan nama dan email
    alert("Login berhasil!\nNama: " + nama + "\nEmail: " + email);
    
    return true;
}

// Tambahkan event listener pada saat halaman dimuat
window.onload = function() {
    // Dapatkan form dan tambahkan event listener untuk submit
    var form = document.getElementById('formtes');
    form.onsubmit = Validasi;
};
